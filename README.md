# lepretre-sandras

SAÉ B - Déployer et sécuriser des services dans un réseau
=========================================================

*   1 Scénario
*   2 Infrastructure et technologies
*   3 Travail attendu
    *   3.1 scripts de création de l’infrastructure
    *   3.2 scripts de déploiement client
*   4 Méthodologie

1 / Scénario
==========

On trouve assez facilement des offres d’hébergement de la solution Odoo permettant aux entreprises de se décharger des questions d’installation, de paramétrage et de maintenance (par exemple [ici](https://www.ethersys.fr/cas-specifiques/odoo/) et [ici](https://www.hebergement-odoo.fr/)).

Globalement, sur ces offres on retrouve :

*   la possibilité d’avoir une installation d’Odoo dans une version spécifique
*   une isolation des différents clients (chacun son installation)
*   un accès HTTPS
*   une sauvegarde des données
*   la sécurité

Dans le cadre de cette SAÉ, on vous propose de vous mettre en position d’hébergeur. Vous devrez donc mettre en place une infrastructure permettant de réaliser l’hébergement de différents serveurs Odoo pour des clients. Dans la mesure où vous pourrez être amenés à réaliser plusieurs installations d’Odoo en fonction des besoins des clients, il devient primordial d’automatiser le processus de déploiment. **L’élément central de cette SAÉ sera donc la production de scripts shell permettant ce déploiment automatique**.

2 / Infrastructure et technologies
================================

La figure ci-dessous vous présente l’infrastructure à mettre en place qui est assez similaire à celle de la SAÉ précédente.

![](schema.svg)

Les choix d’infastructure sont les suivants :

*   Le serveur de base donnée hébergera une base PostgreSQL. Chaque client aura une base spécifique pour son instance Odoo
*   Le serveur Odoo sera utilisé pour le déploiement des différents Odoo des clients sous forme de containers docker
*   Le reverse proxy choisi est [traefik](https://traefik.io/traefik/). Celui-ci permettra un accès différencié pour chaque client avec une URL du type _nomclient.virtu.iutinfo.fr_
*   La base de données sera sauvegardée périodiquement (_dump_ des bases) et copie sur le serveur de sauvegarde (voir _rsync_)

La sécurisation doit être prise en compte :

*   HTTPS sera activé sur le reverse proxy (voir mkcert)
*   Les instances Odoo et la base de données ne sont pas accessibles directement et les connexions vers la base de données seront reduites au strict nécessaire (Odoo client -> base client)

3 / Travail attendu
=================

Le travail sera réalisé en binômes.

Vous utiliserez les mêmes machines de virtualisation que pour la SAÉ précédente.

Vous devrez produire des scripts shell pour automatiser le déploiement ainsi que les procédures d’utilisation de ces scripts. Les scripts devront fonctionner au maximum sans intervention humaine en vous basant sur les paramètres passés et/ou des fichiers de configuration

Vous aurez deux grandes catégories de scripts :

3.1 scripts de création de l’infrastructure
-------------------------------------------

Ce ou ces scripts ont pour objectif de créer et configurer les machines virtuelles qui constituent l’infrastructure globale (Serveur proxy, Serveur Odoo…).

Ces scripts seront utiles par exemple dans le cadre d’un [Plan de Reprise d’Activité (PRA)](https://fr.wikipedia.org/wiki/Plan_de_reprise_d%27activit%C3%A9_(informatique)) pour reconstruire rapidement votre infrastructure en cas d’incident.

3.2 scripts de déploiement client
---------------------------------

Ce ou ces scripts servent à réaliser le déploiement d’un nouveau client. À l’issue de l’exécution vous devriez avoir :

*   un utilisateur et une base spécifique pour le client
*   un Odoo déployé avec les modules choisis par le client (connecté à la base)
*   La sauvegarde prendra en compte la nouvelle base

4 / Méthodologie
==============

Lors de la SAÉ précédente vous avez été guidés dans une méthodologie de maquettage visant à :

1.  Installer les services individuellement pour voir comment ils fonctionnenent et comment ils se configurent
2.  La mise en relation progressive des différents services de manière à comprendre comment ils interagissent et la configuration nécessaire
3.  Un déploiment complet correspondant aux attentes et objectifs initiaux

Vous êtes encouragés à utiliser le même type de méthodologie pour progresser dans cette SAÉ. Vous pouvez si vous le souhaitez utiliser les _milestones_, _issues_ et _boards_ de gitlab pour organiser votre travail.
