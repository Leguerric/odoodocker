#!/bin/bash

#___________________________________
#Prévention de suppression

echo "Si vous avez des vm nommée BD, Save ou ServOdoo, elles seront supprimées et remplacées par de nouvelle."
echo "Voulez vous continuer comme même ? [y/n]"

read rep

#while ([ $rep!="y" ] || [ $rep!="n" ])
#do
#	echo "Voulez vous continuer comme même ? (taper soit y pour oui soit n pour non)"
#	read rep
#done

if [ $rep = "n" ]
then
	 exit
fi
#____________________________________
# Suppression des vm

vmiut rm BD
vmiut rm ServOdoo
vmiut rm Save

#____________________________________

vmiut make Save
vmiut start Save

#attendre que ip soit visible 
SaveIP=$(vmiut info Save | grep 'ip-possible' |cut -c 13- )
while [ -z $SaveIP ]
do
        sleep 5
        SaveIP=$(vmiut info Save | grep 'ip-possible' |cut -c 13- )
done

#envoie script config par ssh
scp address.sh user@$SaveIP:/tmp/
ssh -t user@$SaveIP 'su -c "/./tmp/address.sh Save 105"'


#___________________________________

vmiut make BD
vmiut start BD


#attendre que ip soit visible
BDIP=$(vmiut info BD | grep 'ip-possible' |cut -c 13- ) 
while [ -z $BDIP ]
do 
	sleep 5
	BDIP=$(vmiut info BD | grep 'ip-possible' |cut -c 13- )
done

#envoie script config par ssh
scp address.sh user@$BDIP:/tmp/
ssh -t user@$BDIP 'su -c "/./tmp/address.sh BD 104"'

sleep 50
scp Saveconf.sh user@192.168.194.104:/home/user/

scp BDconf.sh user@192.168.194.104:/tmp/
ssh -t user@192.168.194.104 'su -c "/./tmp/BDconf.sh"'


#____________________________________

vmiut -d /home/public/vm/disque-bullseye-11.6-10go.vdi make ServOdoo
vmiut start ServOdoo

#attendre que ip soit visible 
ServOdooIP=$(vmiut info ServOdoo | grep 'ip-possible' |cut -c 13- )
while [ -z $ServOdooIP ]
do
	sleep 5
	ServOdooIP=$(vmiut info ServOdoo | grep 'ip-possible' |cut -c 13- )
done

#envoie script config par ssh
scp address.sh user@$ServOdooIP:/tmp/
ssh -t user@$ServOdooIP 'su -c "/./tmp/address.sh ServOdoo 103"'

sleep 50

scp RProxyconf.sh user@192.168.194.103:/tmp/
ssh -t user@192.168.194.103 'su -c "/./tmp/RProxyconf.sh"'

scp ServOdooconf.sh user@192.168.194.103:/home/user/
ssh -t user@192.168.194.103 'su -c "/./home/user/ServOdooconf.sh odoo1 8069 '$HOSTNAME'"'

#___________________________________

ssh -f -N -L $HOSTNAME:9090:192.168.194.103:8080 -L $HOSTNAME:9091:192.168.194.103:8081 user@192.168.194.103
