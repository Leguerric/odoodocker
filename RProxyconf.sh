#!/bin/bash

apt update
apt upgrade

apt install docker docker-compose

#faire la config de docker pour le proxy et mirror
mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"HTTP_PROXY=http://cache.univ-lille.fr:3128\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"HTTPS_PROXY=http://cache.univ-lille.fr:3128\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"http_proxy=http://cache.univ-lille.fr:3128\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"https_proxy=http://cache.univ-lille.fr:3128\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"NO_PROXY=localhost,192.168.194.0/24,172.18.48.0/22\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf

#config du mirror ne fonctionne pas
#echo "\"registry-mirrors\": [\"http://172.18.48.9:5000\"]," >> /etc/docker/daemon.json
#echo "\"default-address-pools\":" >> /etc/docker/daemon.json
#echo "  [" >> /etc/docker/daemon.json
#echo "    {\"base\":\"172.20.0.0/16\",\"size\":24}" >> /etc/docker/daemon.json
#echo "  ]" >> /etc/docker/daemon.json

systemctl daemon-reload
systemctl restart docker

docker network create traefik_net

mkdir traefik
cd traefik
touch docker-compose.yml

# création de traefik
#metre les bon truc dans docker-compose de traefik

echo "version: '3'" >> docker-compose.yml
echo "services:" >> docker-compose.yml
echo "  reverse-proxy:" >> docker-compose.yml
echo "    # The official v2 Traefik docker image" >> docker-compose.yml
echo "    image: traefik:v2.9" >> docker-compose.yml
echo "    # Enables the web UI and tells Traefik to listen to docker" >> docker-compose.yml
echo "    command: --api.insecure=true --providers.docker" >> docker-compose.yml
echo "    networks:" >> docker-compose.yml
echo "      - traefik_net" >> docker-compose.yml
echo "    ports:" >> docker-compose.yml
echo "      # The HTTP port" >> docker-compose.yml
echo "      - "8081:80"" >> docker-compose.yml
echo "      # The Web UI (enabled by --api.insecure=true)" >> docker-compose.yml
echo "      - "8080:8080"" >> docker-compose.yml
echo "    volumes:" >> docker-compose.yml
echo "      # So that Traefik can listen to the Docker events" >> docker-compose.yml
echo "      - /var/run/docker.sock:/var/run/docker.sock" >> docker-compose.yml
echo "networks:" >> docker-compose.yml
echo "  traefik_net:" >> docker-compose.yml
echo "    external: true" >> docker-compose.yml

docker-compose up -d
cd ../
