#!/bin/bash
if [[ $# -ne 2 ]] ; 
then 
	echo "il faut 2 parametre, le nom de l'instance et le port" 
	exit 1 
fi
#création de l'utilisateur sur BD
ssh -t user@192.168.194.104 'su -c "/./home/user/NewUser.sh '$1'"'

#Création de l'instance odoo sur ServOdoo
ssh -t user@192.168.194.103 'su -c "/./home/user/ServOdooconf.sh '$1' '$2' '$HOSTNAME'"'
