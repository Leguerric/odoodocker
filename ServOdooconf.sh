#!/bin/bash
# 1er parametre nom du conteneur, 2eme parametre le port, 3eme nom de la machine physique

# création de odoo
if [[ -e $1 ]] ; 
then 
	echo "l'instance $1 existe déja !" 
	exit 1 
fi

mkdir $1
cd $1
touch docker-compose.yml

echo "version: '2'" >> docker-compose.yml
echo "services:" >> docker-compose.yml
echo "  $1:" >> docker-compose.yml
echo "    image: odoo:14" >> docker-compose.yml
echo "    user: root" >> docker-compose.yml
echo "    networks:" >> docker-compose.yml
echo "      - traefik_net" >> docker-compose.yml
echo "    ports:" >> docker-compose.yml
echo "      - $2:8069" >> docker-compose.yml
echo "    tty: true" >> docker-compose.yml
echo "    command: --" >> docker-compose.yml
echo "#    command: odoo scaffold /mnt/extra-addons/custom_module" >> docker-compose.yml
echo "    labels:" >> docker-compose.yml
echo "      - \"traefik.enable=true\"" >> docker-compose.yml
echo "      - \"traefik.http.routers.$1.rule=Host(\`$1.$3.iutinfo.fr\`)\"" >> docker-compose.yml
echo "    environment:" >> docker-compose.yml
echo "      - HOST=192.168.194.104" >> docker-compose.yml
echo "      - USER=$1" >> docker-compose.yml
echo "      - PASSWORD=odoo" >> docker-compose.yml
echo "    volumes:" >> docker-compose.yml
echo "      #- /etc/timezone:/etc/timezone:ro" >> docker-compose.yml
echo "      #- /etc/localtime:/etc/localtime:ro" >> docker-compose.yml
echo "      # - ./entrypoint.sh:/entrypoint.sh   # if you want to install additional Python packag>" >> docker-compose.yml
echo "      - ./addons:/mnt/extra-addons" >> docker-compose.yml
echo "      - ./etc:/etc/odoo" >> docker-compose.yml
echo "    restart: always" >> docker-compose.yml
echo "networks:" >> docker-compose.yml
echo "  traefik_net:" >> docker-compose.yml
echo "    external: true" >> docker-compose.yml

docker-compose up -d
cd ../
