#!/bin/bash

apt update && apt full-upgrade

apt install postgresql

# création de l'user odoo
su - postgres -c "psql -c \"CREATE USER odoo1 WITH PASSWORD 'odoo';\""
su - postgres -c "psql -c \"alter user odoo1 with CREATEDB;\""

#dans postgresql.conf 
# TODO : tester si ca fonctionne meme si la ligne est a la fin
# TODO : tester en mettan l'ip de odoo
echo "listen_addresses = '*'" >> /etc/postgresql/13/main/postgresql.conf

#dans pg_hba.conf
#(IPv4 local connections) access differente base client.
echo "host all all all md5" >> /etc/postgresql/13/main/pg_hba.conf

systemctl restart postgresql

echo "0 */2 * * * ./home/user/Saveconf.sh" >> /var/spool/cron/crontabs/root

#création d'un script pour ajouter un user a la BD
echo "#!/bin/bash" >> /home/user/NewUser.sh
echo "su - postgres -c \"psql -c \\\"CREATE USER \$1 WITH PASSWORD 'odoo';\\\"\"" >> /home/user/NewUser.sh
echo "su - postgres -c \"psql -c \\\"alter user \$1 with CREATEDB;\\\"\"" >> /home/user/NewUser.sh
chmod u+x /home/user/NewUser.sh
