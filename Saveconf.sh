#!/bin/bash

d=$(date +"%Y-%m-%d")

su - postgres -c "pg_dumpall -f /tmp/save_$d"
scp /tmp/save_$d user@192.168.194.105:/home/user/save
