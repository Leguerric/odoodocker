#!/bin/bash

if [ $1 = "Save" ]
then
	su - user -c "mkdir /home/user/save"
fi

sed -i s/dhcp/static/ /etc/network/interfaces
echo address 192.168.194.$2/24 >> /etc/network/interfaces
echo gateway 192.168.194.2 >> /etc/network/interfaces

echo $1 > /etc/hostname
sed -i s/debian/$1/ /etc/hosts

echo "NTP=ntp.univ-lille.fr" >> /etc/systemd/timesyncd.conf

echo HTTP_PROXY=http://cache.univ-lille.fr:3128 >> /etc/environment
echo HTTPS_PROXY=http://cache.univ-lille.fr:3128 >> /etc/environment
echo http_proxy=http://cache.univ-lille.fr:3128 >> /etc/environment
echo https_proxy=http://cache.univ-lille.fr:3128 >> /etc/environment
echo NO_PROXY=localhost,192.168.194.0/24,172.18.48.0/22 >> /etc/environment

systemctl reboot
